========================================
 README for Adélie Linux Build Software
========================================
:Authors:
  * **A. Wilcox**, overall project lead
  * **Max Rees**, considerable feature development and improvements
  * **Adélie Linux Developers and Users**, contributions
:Status:
  Production
:Copyright:
  © 2019-2020 Adélie Linux Team.  Mix of MIT and GPL 2 open source licences.




Introduction
============

This repository contains the software used to build the Adélie Linux package
set.  It is used by Adélie Linux for package building to create the repository
used by Adélie's APK package manager.

This is a *hard fork* of Alpine's ``abuild`` package, with notable and
significant improvements:

* Recursive mode is retained.

* Forced secure mode for ``abuild-fetch``, preventing MITM attacks during
  source fetch.

* More CPU architectures are supported.

* Better (though still incomplete) support for non-Bash shells.

More improvements are forthcoming.


Licenses
`````````
Most of the code in this repository is licensed GPL-2.0-only.  The
``abuild-fetch`` applet is licensed under the MIT license.


Changes
```````
Any changes to this repository - additions, removal, or version bumps - must
be reviewed before being pushed to the master branch.  There are no exceptions
to this rule.  For security-sensitive updates, contact the Security Team at
sec-bugs@adelielinux.org.




Contents
========

This section contains a high-level view of the contents of this repository.


``abuild-fetch``: Download package sources
``````````````````````````````````````````
The ``abuild-fetch`` applet is used to download package sources from the
Internet.


``abuild-tar``: Archive manipulation
````````````````````````````````````
The ``abuild-tar`` applet is used to manipulate ``tar`` files into ``APK``
files.  More on the APK file format is discussed on our wiki_.

.. _wiki: https://wiki.adelielinux.org/wiki/APK_internals


``abuild``: Package building
````````````````````````````
The ``abuild`` script is the script responsible for building packages.


``newapkbuild``: Package recipe creation
````````````````````````````````````````
The ``newapkbuild`` script is used to create new package recipes, called
``APKBUILD`` files.  More information can be found in the Handbook_ or the
man page installed with the ``abuild-doc`` package (``man 5 APKBUILD``).

.. _Handbook: https://help.adelielinux.org/html/devel/

